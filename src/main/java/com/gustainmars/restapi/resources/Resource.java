package com.gustainmars.restapi.resources;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/")
public class Resource {

    @GetMapping("/listaReversa")
    public int[] listaReversa(@RequestParam(value = "lista", defaultValue = "1,2,3") int[] num) {
        
        int l = num.length;
        int mun[] = new int[l];
        int aux = l;
        
        for(int i = 0; i < l; i++) {
            
            mun[i] = num[aux - 1];
            aux = aux - 1;
        }
        return mun;
    }

    @GetMapping("/imprimirImpares")
    public ArrayList<Integer> imprimirImpares(@RequestParam(value = "lista", defaultValue = "1,2,3,4,5") int[] num) {
        
        int l = num.length;
        ArrayList<Integer> impares;
        impares = new ArrayList<>();
        
        for(int i = 0; i < l; i++) {
            
            if(num[i] % 2 != 0)
                impares.add(new Integer(num[i]));
        }
        return impares;
    }
    
    @GetMapping("/imprimirPares")
    public ArrayList<Integer> imprimirPares(@RequestParam(value = "lista", defaultValue = "1,2,3,4,5") int[] num) {
        
        int l = num.length;
        ArrayList<Integer> pares;
        pares = new ArrayList<>();
        
        for(int i = 0; i < l; i++) {
            
            if(num[i] % 2 == 0)
                pares.add(new Integer(num[i]));
        }
        return pares;
    }

    @GetMapping("/tamanho")
    public String tamanho(@RequestParam(value = "palavra", defaultValue = "Ola, Mundo!") char[] palavra) {
        
        int l = palavra.length;
        return "tamanho="+l;
    }

    @GetMapping("/maiusculas")
    public String maiusculas(@RequestParam(value = "palavra", defaultValue = "Ola, Mundo!") String palavra) {
        
        String palavraMaiuscula = palavra.toUpperCase();
        return palavraMaiuscula;
    }

    @GetMapping("/vogais")
    public String vogais(@RequestParam(value = "palavra", defaultValue = "Ola, Mundo!") String palavra) {
        
        palavra = palavra.toLowerCase();
        String vogais = new String();
        for(int i = 0; i < palavra.length(); i++)
            if(palavra.charAt(i) == 'a' || palavra.charAt(i) == 'e' 
            || palavra.charAt(i) == 'i' || palavra.charAt(i) == 'o' || palavra.charAt(i) == 'u')
                vogais += palavra.charAt(i);
        return vogais;
    }

    @GetMapping("/consoantes")
    public String consoantes(@RequestParam(value = "palavra", defaultValue = "Ola, Mundo!") String palavra) {
        
        palavra = palavra.toLowerCase();
        String consoantes = new String();
        for(int i = 0; i < palavra.length(); i++)
            if(palavra.charAt(i) != 'a' && palavra.charAt(i) != 'e' 
            && palavra.charAt(i) != 'i' && palavra.charAt(i) != 'o' && palavra.charAt(i) != 'u')
                consoantes += palavra.charAt(i);
        return consoantes;
    }

    @GetMapping("/nomeBibliografico")
    public String nomeBibliografico(@RequestParam(value = "nome", defaultValue = "João%da%Silva%Corrêa") String nome) {
                
        int iniSobreNome = nome.lastIndexOf("%") + 1;
        int fimPrimNome = nome.indexOf('%');
        String sobreNome = nome.substring(iniSobreNome, nome.length());
        sobreNome = sobreNome.toUpperCase();

        String primNome = nome.substring(0, fimPrimNome);
        String p1 = primNome.substring(0,1).toUpperCase();
        primNome = p1 + primNome.substring(1);

        String segNome = nome.substring(fimPrimNome + 1, iniSobreNome - 1);
        String s1 = segNome.substring(0,1).toUpperCase();
        segNome = s1 + segNome.substring(1);

        String nomeB = sobreNome + ", " + primNome + " " + segNome;
        
        if(nomeB.contains("%")) {
            for(int i = 0; i < nomeB.length(); i++) {
                if(nomeB.charAt(i) == '%')
                    nomeB = nomeB.substring(0, i) + " " + nomeB.substring(i + 1, nomeB.length());
            }
        }

        return nomeB;
    }

    @GetMapping("/sistemaMonetario")
    public String sistemaMonetario(@RequestParam(value = "valorSaque", defaultValue = "8") int valorSaque) {
        
        String notas = new String();
        int resto = valorSaque;
        int nota3 = 0, nota5 = 0;                
        
        if(resto != 0) {

            for(; resto >= 5;){
                nota5++;
                resto -= 5;          
            }
            for(; resto >= 3;) {
                nota3++;
                resto -= 3;            
            }
        }
        else {
            return "Favor digitar um valor de saque maior que " + resto + "!";
        }

        notas = "Para sacar o valor de R$ " + valorSaque + 
        ",00 será entregue " + nota3 + " notas de R$ 3,00 e " + nota5 +
        " notas de R$ 5,00.";
        return notas;
     }
}